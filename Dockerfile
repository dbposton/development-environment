FROM ubuntu:bionic as baseos
MAINTAINER DBPoston <daniel.poston@newspring.cc>
# Update Base System
#
ENV  GO_VERSION=1.10.3
#
WORKDIR /tmp
#
RUN  sed -i -e 's/^deb-src/#deb-src/' /etc/apt/sources.list \
     && export DEBIAN_FRONTEND=noninteractive \
     && apt-get update \
     && apt-get upgrade -y --no-install-recommends \
     && apt-get install -y --no-install-recommends \
     apt-transport-https \
     ca-certificates \
     curl \
     dnsutils \
     git \
     gnupg2 \
     htop \
     less \
     locales \
     lynx \
     mtr \
     nmap \
     openssh-client \
     p7zip-full \
     python \
     tmux \
     vim \
     zsh \
     && locale-gen en_US.UTF-8 \
     && curl -L https://storage.googleapis.com/golang/go$GO_VERSION.linux-amd64.tar.gz | tar -C /usr/local -xz \
     && apt-get autoremove -y \
     && apt-get clean -y \
     && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/share/man /usr/share/doc
#
WORKDIR /root
#
RUN git clone https://gitlab.com/prov47tech/dotfiles.git
#
RUN /root/dotfiles/install.sh
#
RUN rm -rf /root/dotfiles
#
CMD ["zsh"]